//
//  Colors.swift
//  ARBlock
//
//  Created by Yashvardhan Mulki on 2018-06-05.
//  Copyright © 2018 Yashvardhan Mulki. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
    static let flatWhiteLight = UIColor(red: 236, green: 240, blue: 241, alpha: 1)
    static let flatNavyBlueLight = UIColor(red: 54, green: 73, blue: 95, alpha: 1)
}
