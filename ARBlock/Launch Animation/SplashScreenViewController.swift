//
//  SplashScreenViewController.swift
//  ARBlock
//
//  Created by Yashvardhan Mulki on 2018-06-05.
//  Copyright © 2018 Yashvardhan Mulki. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    let impactGenerator = UIImpactFeedbackGenerator()
    // Count number of animation cycles
    var animationsRun = 0
    // Current angle of imageView in radians
    var currentAngle = 0.0
    // 90 Degree Spin
    let ninetyDegrees = 1.57
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    /// Performs a spin animation on the image view
    ///
    /// - Parameter duration: duration in seconds
    private func spinAnimation(duration:Double) {
        UIView.animate(withDuration: TimeInterval(duration), animations: {
            self.logoImageView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.currentAngle += self.ninetyDegrees
            self.logoImageView.transform = CGAffineTransform(rotationAngle: CGFloat(self.currentAngle))
            self.logoImageView.transform = CGAffineTransform.identity
            self.currentAngle += self.ninetyDegrees
            self.logoImageView.transform = CGAffineTransform(rotationAngle: CGFloat(self.currentAngle))
        }) { (completed) in
            // Haptic Effect
            let impact = UIImpactFeedbackGenerator()
            impact.impactOccurred()
            
            self.animationsRun += 1
            if self.animationsRun <= 1 {
                self.spinAnimation(duration: duration)
            } else {
                
            }
        }
    }

}
